<?php
/* database configuration */
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "toor";

/* create connection object */
$connection = mysql_connect($dbhost, $dbuser, $dbpass);

if(!$connection){
  die("err: Couldn't connect to the SQL server: " . mysql_error());
}

// you're now connected!

/* SQL to create 'sites' database */
$sql1 = "CREATE DATABASE sites";
$retval = mysql_query($sql1, $connection);

if(!$retval){
  die("Error creating 'sites' database: " . mysql_error());
}

echo "'sites' database created successfuly!<br>";

/* use databse */
mysql_select_db('sites');

/* SQL to create 'fakesites' table */
$sql2 = "CREATE TABLE fakesites(" .
"domain VARCHAR(225) NOT NULL, " .
"url TEXT NOT NULL, " .
"conttype VARCHAR(50) NOT NULL, " .
"status VARCHAR(5) NOT NULL, " .
"score INT NOT NULL, " .
"issue TEXT".
")";

$retval = mysql_query($sql2, $connection);

if(!$retval){
  die("Error creating 'fakesites' table: " . mysql_error());
}

echo "'fakesites' table created successfuly!<br>";
?>